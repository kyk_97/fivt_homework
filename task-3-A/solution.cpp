
#pragma once

#include <condition_variable>
#include <deque>
#include <iostream>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>

class MyException: public std::exception {
public:
    MyException(): std::exception() {
    }
};

template <class T, class Container = std::deque<T>>
class BlockingQueue {
public:

    explicit BlockingQueue(const size_t& capacity)
        : capacity_(capacity),
          is_active_(true) {
    }

    void Put(T&& element) {
        std::unique_lock<std::mutex> lock(mtx_);
        while (queue_of_elements_.size() == capacity_ && is_active_) {
            vacancy_cv_.wait(lock);
        }
        if (is_active_ && queue_of_elements_.size() != capacity_) {
            queue_of_elements_.push_back(std::move(element));
            add_element_cv_.notify_one();
        }
        if (!is_active_) {
            throw MyException();
        }
    }

    bool Get(T& result) {
        std::unique_lock<std::mutex> lock(mtx_);
        while (queue_of_elements_.empty() && is_active_) {
            add_element_cv_.wait(lock);
        }
        if (is_active_ && !queue_of_elements_.empty()) {
            result = std::move(queue_of_elements_.front());
            queue_of_elements_.pop_front();
            vacancy_cv_.notify_one();
            return true;
        } else {
            return false;
        }
    }

    void Shutdown() {
        std::unique_lock<std::mutex> lock(mtx_);
        is_active_ = false;
        vacancy_cv_.notify_all();
    }

private:
    const size_t capacity_;
    bool is_active_;
    Container queue_of_elements_;
    std::condition_variable vacancy_cv_;
    std::condition_variable add_element_cv_;
    std::mutex mtx_;
};

