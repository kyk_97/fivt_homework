#Очередь
Данная реализация очереди не является линеаризуемой.
Приведу пример, когда операции не могут быть линеаризованы:
T1.enqueue(1)::start; //Переводит tail но не провязывает новый хвост со старым, засыпает.
T2.enqueue(2)::start and end; // Добавил елемент 2 после элемента 1.
T2.dequeue()::start, return false, end; // Посмотрел на фиктивный узел, ссылки на элемент 1 еще нет, 
					// подумал что в очерди нет ничего и вернул false
T1.enqueue(1)::end; // завершился

Так как dequeue() вызывался после enqueue(2), то он должен был удалить хотя бы один элемент в данной ситуации.
А вызов dequeue() завершился с false. Данная история не линеаризуема.
